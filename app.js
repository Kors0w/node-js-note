const fs = require('fs');
const chalk = require('chalk')

function read(callback) {
    fs.readFile(`${__dirname}/datas/notes.json`,'UTF-8',(error, data)=> {
        data = error ? [] : JSON.parse(data)
        callback(error,data)
    })
}

function add(note) {
    read((error, data) => {

        if (data.find((item) => item.title === note.title)){
            console.log(chalk.red.bold `le fichier contient déjà une note avec le même titre.`)
            return;
        }
        data.push(note)

        push(data, ()=> {
            console.log(chalk.green `note ajouté`)
        })
    })
}

function remove(title) {
    read((error,data) => {
        let size = data.length;

        data = data.filter((item) => item.title !== title)

        push(data, ()=> {
            console.log(data.length === size ? chalk.red.bold `La note ${title} n'éxiste pas` : chalk.green `la note ${title} a été supprimée`)
        })
    })
}


function push(data, callback){
    fs.writeFile(`${__dirname}/datas/notes.json`,JSON.stringify(data), (err => {
        if (err)
            throw err;
        callback()
    }))
}


yargs = require("yargs")


yargs.command({
    command: 'add',
    describe: 'Ajouter une note',
    builder: {
        title: {
            describe: 'title of the new note',
            demandOption: true,
            type: 'string',
        },
        content: {
            describe: 'content of the note',
            demandOption: true,
            type: 'string',
        }
    },
    handler: (argv) => {
        add({title: argv.title, content: argv.content})
    }
}).command({
    command: 'list',
    describe: 'display the notes',
    handler: () => {
        read((error,data) => {
            data.forEach(note => {
                console.log(chalk.blue `titre : " ${note.title}, content: ${note.content}`)
            })
        })
    }
}).command({
    command: 'remove',
    describe: 'remove a note',
    builder: {
        title: {
            describe: 'title of the note you wanna remove',
            demandOption: true,
            type: 'string',
        }
    },
    handler: (argv) => {
        remove(argv.title)
    }
}).argv
